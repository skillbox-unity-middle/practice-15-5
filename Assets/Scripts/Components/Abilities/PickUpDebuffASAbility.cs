using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PickUpDebuffASAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    [SerializeField] private float _debuffShootDelay = 0.5f;
    private EntityManager _dstManager;

    public List<GameObject> Targets { get; set; }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (_debuffShootDelay != 0)
            {
                var shootAbility = target.GetComponent<ShootAbility>();
                if (shootAbility == null) return;
                shootAbility.ShootDelayMultiplier = _debuffShootDelay;
                _debuffShootDelay = 0;
            }
            Destroy(gameObject);
            _dstManager.DestroyEntity(entity);
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
    }
}
