using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyGameBalance : MonoBehaviour, ISettings
{
    private int _heroHealth = 120;
    private int _recoverHealth = 30;

    public int HeroHealth => _heroHealth;
    public int RecoverHealth => _recoverHealth;

    public event Action onLoadSettings;

    private void Start()
    {
        onLoadSettings?.Invoke();
    }
}
