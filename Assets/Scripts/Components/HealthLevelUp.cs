﻿using UnityEngine;

public class HealthLevelUp : MonoBehaviour, ILevelUp
{
    private CharacterHealthConvertToEntity _health;
    [SerializeField] private int _hpPerLevel = 20;

    private void Start()
    {
        _health = GetComponent<CharacterHealthConvertToEntity>();
    }

    public void LevelUp(CharacterData data, int level)
    {
        if (_health == null)
        {
            _health = GetComponent<CharacterHealthConvertToEntity>();
            if (_health != null) return;
        }
        int deltaHp = _hpPerLevel;
        _health.SetMaxHp(deltaHp);
    }
}